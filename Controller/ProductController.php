<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{

    public function index()
    {
        return view('admin.product.index');
    }

    public function getProducts(){

        $products = Product::withoutTrashed()->get();

        return Datatables::of($products)->addColumn('action', function ($products) {
            return '<a href="' . route("products.show", ['product'=> $products]) . '" class="btn btn-danger dropdown-toggle btn-md">Show</a>';
        })->addColumn('category', function ($products) {
            return isset($products->category->name) ? $products->category->name : 'Deleted (This product will not be listed!)';
        })->addColumn('price', function ($products) {
            return $products->price_money_format;
        })->addColumn('discount', function ($products) {
            return $products->discount_with_percent;
        })->make(true);
    }


    public function create()
    {
        $categories = Category::withoutTrashed()->get(['id','name']);

        return view('admin.product.create', compact('categories'));
    }


    public function store(ProductRequest $request)
    {
        $request->validated();

        try {

            $response = [
                'status' => 'success',
                'message' => 'Product successfully stored'
            ];

            if(!$request->has('image')){

                throw new \Exception('api-unknown_error');
            }

            $image = $request->file('image');

            $fileName = $this->uploadImage($image);

            $createData = $request->merge([
                'cover_photo' => $fileName,
                'created_by' => Auth::user()->id
            ])->except(['_token']);

            Product::create($createData);

            return redirect()->back()->with($response['status'], $response['message']);

        } catch (\Illuminate\Database\QueryException $exception) {
            $message = $exception->getMessage();
            Log::debug($message);
            $response = [
                'status' => 'error',
                'message' => 'Product Store failed!'
            ];
        }catch (\Exception $e) {
            $message = $e->getFile() . " " . $e->getLine() . " " . $e->getMessage();
            Log::error($message);
            $response['message'] = $e->getMessage();
        }

        return redirect()->back()->with($response['status'], $response['message'])
            ->withInput();
    }

    private function uploadImage($image){

        $fileName = md5(time()).'.'.$image->extension();

        Image::make($image)->resize(450,550)->save(public_path('/upload/product/'.$fileName));

        return $fileName;
    }


    public function show(Product $product)
    {
        return view('admin.product.show', compact('product'));
    }


    public function edit(Product $product)
    {
        $categories = Category::withoutTrashed()->get(['id','name']);

        return view('admin.product.edit', compact('product', 'categories'));
    }


    public function update(ProductRequest $request, Product $product)
    {

        $request->validated();

        try {

            $response = [
                'status' => 'success',
                'message' => 'Product successfully updated'
            ];

            if ($request->hasFile('image')) {

                $image = $request->file('image');
                $fileName = $this->uploadImage($image);

                $updateData = $request->merge([
                    'cover_photo' => $fileName,
                    'updated_by' => Auth::user()->id
                ])->except(['_token', 'image']);


                $product->update($updateData);

            }else {

                $updateData = $request->merge([
                    'updated_by' => Auth::user()->id
                ])->except(['_token', 'image']);

                $product->update($updateData);

            }

        }catch (\Exception $e) {
            $message = $e->getFile() . " " . $e->getLine() . " " . $e->getMessage();
            Log::error($message);
            $response['message'] = $e->getMessage();
        }

        return redirect()->back()->with($response['status'], $response['message']);

    }


    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index');
    }
}
