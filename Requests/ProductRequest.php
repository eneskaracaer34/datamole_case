<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function rules()
    {
        return [
            'category_id' => 'required',
            'name' => 'required',
            'detail' => 'required',
            'price' => 'required|numeric|between:0,500.00',
            'stock' => 'required|numeric|min:1',
            'discount' => 'nullable|numeric|min:1|max:90',
            'cover_photo' => 'image|mimes:jpeg,png,jpg',
        ];
    }


    public function messages()
    {
        return [
            'category_id.required' => 'Category Name is required!',
            'name.required' => 'Product Name is required!',
            'detail.required' => 'Product Detail is required!',
            'price.required' => 'Price is required!',
            'stock.required' => 'Stock is required!',
            'discount.max' => 'Discount may not be greater than 90',
            'price.between' => 'Price must be between 0 and 500.00',
            'stock.min' => 'Stock must be at least 1',
            'price.numeric' => 'Price must be a number',
            'stock.numeric' => 'Stock must be a number',
            'discount.numeric' => 'Discount must be a number',
            'discount.min' => 'Discount must be at least 1'
        ];
    }
}
