<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $appends = ['discounted_price', 'price_money_format', 'discount_with_percent', 'money_format'];

    protected $fillable = ['category_id', 'name', 'detail', 'price', 'stock', 'discount', 'cover_photo', 'created_by', 'updated_by'];

    public function getDiscountedPriceAttribute()
    {
        $discountedPrice = $this->price * (100 - $this->discount ) /100;
        return  '$'.number_format($discountedPrice, 2, '.', ',');
    }


    public function getMoneyFormatAttribute()
    {
        return  '$'.number_format($this->price, 2, '.', ',');

    }
    

    public function category()
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }
}
