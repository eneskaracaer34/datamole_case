<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');


Auth::routes();

Route::group(['prefix' => 'shop','middleware' => ['auth'] ], function() {

    Route::get('/', 'Website\ShopController@index')->name('shop');

    Route::resource('cart', 'Website\CartController');

    Route::delete('/cart/destroy-all/{id}', 'Website\CartController@destroyAll')->name('cart.destroy.all');

    Route::get('/cart/{id}/order', 'Website\OrderController@index')->name('order.index');

    Route::post('/order', 'Website\OrderController@store')->name('order.store');

    Route::get('/{category}/{product}/{id}', 'Website\ShopController@detail')->name('product.detail');

});



Route::group(['prefix' => 'home','middleware' => ['admin'] ], function() {

    Route::get('/', 'Admin\HomeController@index')->name('home');

    Route::resource('categories', 'Admin\CategoryController');

    Route::resource('products', 'Admin\ProductController');

    Route::resource('orders', 'Admin\OrderController');

    Route::get('datatable/categories', 'Admin\CategoryController@getCategories')->name('categories.dataTable');

    Route::get('datatable/products', 'Admin\ProductController@getProducts')->name('products.dataTable');

    Route::get('datatable/orders', 'Admin\OrderController@getOrders')->name('orders.dataTable');

});
