<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    const ROLE_ADMIN = "ROLE_ADMIN";

    public function handle($request, Closure $next)
    {
        if (Auth::user() &&  Auth::user()->role->name === self::ROLE_ADMIN) {
            return $next($request);
        }
        abort(403);
    }
}
